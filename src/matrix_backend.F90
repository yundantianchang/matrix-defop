! Copyright 2012      Radovan Bast
!           2009-2012 Andreas J. Thorvaldsen
! This file is made available under the terms of the
! GNU Lesser General Public License version 3.

! Module matrix_backend contains slightly generalized
! matrix operations.
!
! Only intended as hidden back-end for matrix_defop.
module matrix_backend

  implicit none

  public openrsp_matrix
  public mat_nullify
  public mat_init
  public mat_dup
  public mat_move
  public mat_remove
  public mat_is_defined
  public mat_assert_def
  public mat_is_zero
  public mat_is_complex
  public mat_is_quaternion
  public mat_is_closed_shell
  public mat_is_open_shell
  public mat_is_alias
  public mat_get_nrow
  public mat_get_ncol
  public mat_get_part
  public mat_axpy
  public mat_gemm
  public mat_dot
  public mat_trace
  public mat_hide_temp
  public mat_unhide_temp
  public mat_ensure_alloc
  public mat_imag_times_mat
  public mat_acquire_block
  public mat_unacquire_block
  public mat_mpi_bcast
  public matrix_backend_debug
  private
  
  ! matrix structure
  type openrsp_matrix
     integer :: nrow !number of rows (of each block)
     integer :: ncol !number of columns (...)
     integer :: ih_sym  !hermiticity: -1 (anti-hermitian), 0 (nonhermitian), 1 (hermitian)
     integer :: pg_sym  !point group symmetry: 1 to nr of ireps
     integer :: algebra !algebra: 1 (real), 2 (complex), 4 (quaternion)
     ! Tag used to spot accidental use of uninitialized and memory-corrupted
     ! matrices, and fail with something other than 'segmentation fault'.
     ! Set to magic_val_nonz or magic_val_zero by 'init', and zeroed
     integer :: magic_tag !by 'remove' and 'nullify'
     ! Pointer to self, set by init to mark the matrix' correct location.
     ! Used to distinguish init'ed matrices from copy-in'ed matrices like
     ! "call subr((/A,B,C/))". Nullified by 'remove' and 'nullify'
     type(openrsp_matrix), pointer :: self_pointer
     real(8), pointer :: elms(:, :, :) !(nrow,ncol,algebra)
  end type

  integer, parameter :: magic_val_zero =  2035728172
  integer, parameter :: magic_val_nonz = -1981879812
  integer, parameter :: magic_val_temp = -1776576656

  logical :: matrix_backend_debug = .false.
  
  logical, parameter :: T=.true., F=.false.

contains


  ! Scale-add or scale-copy X to Y: Y (+)= a*X^tx.  If += and both X and Y are zero,
  ! ie 0 += a*0^tx, this is a no-op, except that shapes are verified and that Y
  ! will receive any extra component flags from X (quaternion).
  subroutine mat_axpy(a, X, tx, ey, Y)
    real(8),      intent(in)    :: a
    type(openrsp_matrix), intent(in)    :: X
    logical,      intent(in)    :: tx, ey
    type(openrsp_matrix), intent(inout) :: Y
    logical      equals, zero
    type(openrsp_matrix) Yy
    call mat_assert_def(X, 'mat_axpy called with matrix X undefined')
    if (.not.ey) call mat_assert_def(Y, 'mat_axpy called with matrix Y undefined')
    equals = ey !modifiable copy
    zero = (a==0 .or. mat_is_zero(X))
    ! verify and prepare Y
    if (.not.prepare_inout_mat(merge(X%ncol, X%nrow, tx), &
                               merge(X%nrow, X%ncol, tx), zero, &
                               mat_is_quaternion(X), equals, Y)) &
       call quit('mat_axpy called with matrices X^tx and Y with different shapes')
    ! skip, or relay to real or quaternion handler
    if (zero .and. .not.(mat_is_quaternion(X) .and. .not.mat_is_quaternion(Y))) then
       ! all done
    ! X is a zero quaternion, so Y should become quaternion, with zero i, j and k blocks
    else if (zero .and. (mat_is_quaternion(X) .and. .not.mat_is_quaternion(Y))) then
       call mat_init(Yy, Y%nrow, Y%ncol, is_zero=.true.) !zero real closed-shell
       ! will also set quat_j and quatk
       call mat_set_part(Yy, Y, quat_i=T, quat_j=F)
       call mat_set_part(Yy, Y, quat_i=F, quat_j=T)
       call mat_set_part(Yy, Y, quat_i=T, quat_j=T)
    else if (mat_is_quaternion(X) .or. mat_is_quaternion(Y)) then
       ! Yr (+)= a Xr
       Yy = mat_get_part(Y, quat_i=F, quat_j=F)
       call axpy_real(a, mat_get_part(X, quat_i=F, quat_j=F), tx, equals, Yy)
       call mat_set_part(Yy, Y, quat_i=F, quat_j=F)
       ! alloc quat_i part of Y if zero
       Yy = mat_get_part(Y, quat_i=T, quat_j=F)
       if (mat_is_zero(Yy)) equals = .true.
       if (mat_is_zero(Yy)) &
          call mat_init(Yy, Yy%nrow, Yy%ncol)
       ! Yi (+)= a Xi
       call axpy_real(a, mat_get_part(X, quat_i=T, quat_j=F), tx, equals, Yy)
       call mat_set_part(Yy, Y, quat_i=T, quat_j=F)
       ! Yj (+)= a Xj
       Yy = mat_get_part(Y, quat_i=F, quat_j=T)
       call axpy_real(a, mat_get_part(X, quat_i=F, quat_j=T), tx, equals, Yy)
       call mat_set_part(Yy, Y, quat_i=F, quat_j=T)
       ! Yk (+)= a Xk
       Yy = mat_get_part(Y, quat_i=T, quat_j=T)
       call axpy_real(a, mat_get_part(X, quat_i=T, quat_j=T), tx, equals, Yy)
       call mat_set_part(Yy, Y, quat_i=T, quat_j=T)
    else
       call axpy_real(a, X, tx, equals, Y)
    end if
  end subroutine


  ! private per-part axpy
  subroutine axpy_real(a, X, tx, ey, Y)
    real(8),      intent(in)    :: a
    type(openrsp_matrix), intent(in)    :: X
    logical,      intent(in)    :: tx, ey
    type(openrsp_matrix), intent(inout) :: Y
    integer i, j
    real(8) ij, ji
    ! Y=0, Y=a*Y, Y+=a*Y, Y=a*X^T
    if (a==0 .and. ey) then !Y=0
       Y%elms = 0 !zeroing
    else if (a==0) then !Y+=0
       ! nothing
    else if (associated(X%elms, Y%elms)) then !X is Y
       if (tx .and. ey) then
          do j = 1, Y%ncol
             do i = 1, j-1
                ij = Y%elms(i,j,1)
                Y%elms(i,j,1) = a*Y%elms(j,i,1)
                Y%elms(j,i,1) = a*ij
             end do
             Y%elms(j,j,1) = a*Y%elms(j,j,1)
          end do
       else if (tx) then
          do j = 1, Y%ncol
             do i = 1, j-1
                ij = Y%elms(i,j,1)
                ji = Y%elms(j,i,1)
                Y%elms(i,j,1) = ij + a*ji
                Y%elms(j,i,1) = ji + a*ij
             end do
             Y%elms(j,j,1) = (1+a)*Y%elms(j,j,1)
          end do
       else
          ! merge: a if ey else a+1
          Y%elms = merge(a,a+1,ey) * Y%elms
       end if
    else if (tx .and. ey) then
       Y%elms(:,:,1) = a * transpose(X%elms(:,:,1))
    else if (tx) then
       Y%elms(:,:,1) = Y%elms(:,:,1) + a * transpose(X%elms(:,:,1))
    else if (a==1 .and. ey) then
       Y%elms = X%elms
    else if (ey) then
       Y%elms = a*X%elms
    else
       call daxpy(size(Y%elms), a, X%elms, 1, Y%elms, 1)
    end if
  end subroutine

    
  ! matrix multiply: C (+)= f * A^ta * B^tb. (Re-)Allocate C if it is
  ! an alias or has fewer parts than A and B
  subroutine mat_gemm(fab, A, ta, B, tb, ec, C)
    real(8),              intent(in)    :: fab
    type(openrsp_matrix), target, intent(in)    :: A, B
    logical,              intent(in)    :: ta, tb, ec
    type(openrsp_matrix), target, intent(inout) :: C
    logical equals, zero, quaternion
    integer      :: k, m, n
    real(8)      :: alpha, beta
    character(1) :: ha, hb
    equals = ec !modifiable copy
    zero = (fab==0 .or. mat_is_zero(A) .or. mat_is_zero(B))
    quaternion = (mat_is_quaternion(A) .or. mat_is_quaternion(B))
    ! verify that A and B can be multiplied
    if (merge(A%nrow, A%ncol, ta) /= merge(B%ncol, B%nrow, tb)) &
       call quit('mat_gemm called with matrices A^ta and B^tb which cannot be multiplied')
    ! verify and prepare C. merge: A%ncol if ta else A%nrow, etc.
    if (.not.prepare_inout_mat(merge(A%ncol, A%nrow, ta), &
                               merge(B%nrow, B%ncol, tb), &
                               zero, quaternion, equals, C)) &
       call quit('matrix gemm called with A^ta * B^tb which does not fit in C')
    ! skip, or relay to real or quaternion handler
    if (zero .and. .not.(quaternion .and. .not.mat_is_quaternion(C))) then
       ! all done
    else if (quaternion .or. mat_is_quaternion(C)) then

#ifdef PRG_DIRAC
      alpha = fab

      if (ec) then
         beta = 0.0d0
      else
         beta = 1.0d0
      end if

      if (ta) then
         k  = A%nrow
         m  = A%ncol
         ha = 'H'
      else
         m  = A%nrow
         k  = A%ncol
         ha = 'N'
      end if

      if (tb) then
         n  = B%nrow
         hb = 'H'
      else
         n  = B%ncol
         hb = 'N'
      end if

      call qgemm(m, n, k,&
                 alpha,&
                 ha, 'N', (/1, 2, 3, 4/), A%elms, A%nrow, A%ncol, A%algebra, &
                 hb, 'N', (/1, 2, 3, 4/), B%elms, B%nrow, B%ncol, B%algebra, &
                 beta,    (/1, 2, 3, 4/), C%elms, C%nrow, C%ncol, C%algebra)
#else
       ! radovan: this is broken
       call gemm_quaternion
#endif

    else
       call gemm_real(fab, A, B, equals, C)
    end if
  contains
    ! split into parts, multiply parts with gemm_real, join
    subroutine gemm_quaternion
      type(openrsp_matrix) Ar, Ai, Aj, Ak, Br, Bi, Bj, Bk, Cc
      logical eq
      Ar = mat_get_part(A, quat_i=F, quat_j=F)
      Ai = mat_get_part(A, quat_i=T, quat_j=F)
      Aj = mat_get_part(A, quat_i=F, quat_j=T)
      Ak = mat_get_part(A, quat_i=T, quat_j=T)
      Br = mat_get_part(B, quat_i=F, quat_j=F)
      Bi = mat_get_part(B, quat_i=T, quat_j=F)
      Bj = mat_get_part(B, quat_i=F, quat_j=T)
      Bk = mat_get_part(B, quat_i=T, quat_j=T)
      ! Cr (+)= f (Ar Br - Ai Bi - Aj Bj - Ak Bk)
      if (.not.zero) then
         Cc = mat_get_part(C, quat_i=F, quat_j=F)
         call gemm_real( fab, Ar, Br, equals,  Cc)
         call gemm_real(-fab, Ai, Bi, .false., Cc)
         call gemm_real(-fab, Aj, Bj, .false., Cc)
         call gemm_real(-fab, Ak, Bk, .false., Cc)
         call mat_set_part(Cc, C, quat_i=F, quat_j=F)
      end if
      ! if C is real, and now becomes quaternion, Cc will now be zero,
      ! so have it allocated, and instead of zeroing it set equals=true
      Cc = mat_get_part(C, quat_i=T, quat_j=F)
      if (mat_is_zero(Cc)) then
         call mat_init(Cc, Cc%nrow, Cc%ncol)
         equals = .true.
      end if
      ! Ci (+)= f (Ar Bi + Ai Br - Ak Bj + Aj Bk)
      if (.not.zero) then
         call gemm_real( fab, Ar, Bi, equals,  Cc)
         call gemm_real( fab, Ai, Br, .false., Cc) !FIXME symmetry
         call gemm_real(-fab, Ak, Bj, .false., Cc)
         call gemm_real( fab, Aj, Bk, .false., Cc) !FIXME symmetry
      end if
      call mat_set_part(Cc, C, quat_i=T, quat_j=F)
      ! Cj (+)= f (Ar Bj + Aj Br - Ai Bk + Ak Bi)
      Cc = mat_get_part(C, quat_i=F, quat_j=T)
      if (.not.zero) then
         call gemm_real( fab, Ar, Bj, equals,  Cc)
         call gemm_real( fab, Aj, Br, .false., Cc) !FIXME symmetry
         call gemm_real(-fab, Ai, Bk, .false., Cc)
         call gemm_real( fab, Ak, Bi, .false., Cc) !FIXME symmetry
      end if
      call mat_set_part(Cc, C, quat_i=F, quat_j=T)
      ! Ck (+)= f (Ar Bk + Ak Br - Aj Bi + Ai Bj)
      Cc = mat_get_part(C, quat_i=T, quat_j=T)
      if (.not.zero) then
         call gemm_real( fab, Ar, Bk, equals,  Cc)
         call gemm_real( fab, Ak, Br, .false., Cc) !FIXME symmetry
         call gemm_real(-fab, Aj, Bi, .false., Cc)
         call gemm_real( fab, Ai, Bj, .false., Cc) !FIXME symmetry
      end if
      call mat_set_part(Cc, C, quat_i=T, quat_j=T)
      call mat_nullify(Ar)
      call mat_nullify(Ai)
      call mat_nullify(Aj)
      call mat_nullify(Ak)
      call mat_nullify(Br)
      call mat_nullify(Bi)
      call mat_nullify(Bj)
      call mat_nullify(Bk)
    end subroutine
    subroutine gemm_real(fab, A, B, ec, C)
      real(8),      intent(in)    :: fab
      type(openrsp_matrix), intent(in)    :: A, B
      logical,      intent(in)    :: ec
      type(openrsp_matrix), intent(inout) :: C
      call dgemm(merge('T','N',ta), merge('T','N',tb), &
                 C%nrow, C%ncol, merge(A%nrow, A%ncol, ta), &
                 fab, A%elms(:,:,1), A%nrow, B%elms(:,:,1), B%nrow, &
                 merge(0d0,1d0,ec), C%elms(:,:,1), C%nrow)
    end subroutine
  end subroutine


  subroutine mat_init(A,             &
                      nrow,          &
                      ncol,          &
                      is_zero,       &
                      is_complex,    &
                      is_quaternion, &
                      is_open_shell)

    type(openrsp_matrix), intent(out), target  :: A
    integer,      intent(in)           :: nrow
    integer,      intent(in)           :: ncol
    logical,      intent(in), optional :: is_zero
    logical,      intent(in), optional :: is_complex
    logical,      intent(in), optional :: is_quaternion
    logical,      intent(in), optional :: is_open_shell

    logical :: is_not_zero

    if (present(is_complex)) then
       if (is_complex) then
          call quit("mat_init called with 'is_complex' present, but this isn't implemented")
       end if
    end if

    if (present(is_open_shell)) then
       if (is_open_shell) then
          call quit("mat_init called with 'is_open_shell' present, but this isn't implemented")
       end if
    end if

    A%nrow = nrow
    A%ncol = ncol

    is_not_zero = .true.
    if (present(is_zero)) is_not_zero = .not.is_zero
 

    A%ih_sym = 1 !FIXME should be input arg
    A%pg_sym = 1 !FIXME should be input arg

    A%algebra = 1
    if (present(is_quaternion)) then
       if (is_quaternion) then
          A%algebra = 4
       end if
    end if
#ifdef PRG_DIRAC
    A%algebra = 4
#endif

    ! alloc elms according to algebra, except for A=zero
    nullify(A%elms)
    if (is_not_zero) then
      allocate(A%elms(nrow, ncol, A%algebra)) 
    end if

    A%magic_tag = magic_val_zero
    if (is_not_zero) A%magic_tag = magic_val_nonz 

    ! A=zero's self_pointer is null, otherwise back to A 
    nullify(A%self_pointer)

    if (is_not_zero) A%self_pointer => A

  end subroutine
  
  subroutine mat_nullify(A)
    type(openrsp_matrix), target, intent(inout) :: A
    A%nrow = huge(1)
    A%ncol = huge(1)
    A%ih_sym = huge(1)
    A%pg_sym = huge(1)
    A%algebra = huge(1)
    nullify(A%elms)
    A%magic_tag = 0
    nullify(A%self_pointer)
  end subroutine

  ! Deallocate A if non-alias, nullify if alias
  subroutine mat_remove(A)
    type(openrsp_matrix), target, intent(inout) :: A
    call mat_assert_def(A, 'mat_remove(A) called with matrix A undefined')
    if (mat_is_alias(A, A)) deallocate(A%elms)
    call mat_nullify(A)
  end subroutine

  ! 'safely' move matrix A to B. If B is allocated, and A isn't its alias,
  ! free B first. Then overwrite B by A (duplicate), then rest
  subroutine mat_move(A, B)
    type(openrsp_matrix), target, intent(inout) :: A, B
    call mat_assert_def(A, 'mat_move(A,B) called with matrix A undefined')
    if (mat_is_defined(B) .and. .not.mat_is_alias(A, B)) &
       call mat_remove(B)
    call mat_dup(A, B)
    if (mat_is_alias(A, A)) B%self_pointer => B
    call mat_nullify(A)
  end subroutine

  ! fields-wise copy A to B. No book-keeping involved
  subroutine mat_dup(A, B)
    type(openrsp_matrix), target, intent(in)  :: A
    type(openrsp_matrix), target, intent(out) :: B
    B = A
  end subroutine

  ! whether A is defined
  logical function mat_is_defined(A)
    type(openrsp_matrix), intent(in) :: A
    mat_is_defined = (A%magic_tag == magic_val_nonz &
             .or. A%magic_tag == magic_val_zero)
  end function

  ! quit if A isn't defined
  subroutine mat_assert_def(A, errmsg)
    type(openrsp_matrix), target, intent(in) :: A
    character(*),         intent(in) :: errmsg
    if (.not.mat_is_defined(A)) call quit(errmsg)
  end subroutine

  ! whether A is an unallocated zero matrix
  logical function mat_is_zero(A)
    type(openrsp_matrix), intent(in) :: A
    mat_is_zero = (A%magic_tag == magic_val_zero)
  end function

  ! whether A is a complex matrix (Ar Ai)
  logical function mat_is_complex(A)
    type(openrsp_matrix), intent(in) :: A
    mat_is_complex = .false. !FIXME implement complex
  end function

  ! whether A is a quaternion matrix (A0 Ai Aj Ak)
  logical function mat_is_quaternion(A)
    type(openrsp_matrix), intent(in) :: A
    mat_is_quaternion = (A%algebra == 4)
  end function

  ! whether A is a closed shell matrix, for which a dot product
  ! carries an addional factor 2
  logical function mat_is_closed_shell(A)
    type(openrsp_matrix), intent(in) :: A
    mat_is_closed_shell = .true. !FIXME implement non-shell and open-shell
  end function

  ! whether A is an open-shell matrix (Aa Ab). The alpha component will
  ! carries an addional factor 2
  logical function mat_is_open_shell(A)
    type(openrsp_matrix), intent(in) :: A
    mat_is_open_shell = .false. !FIXME implement open-shell
  end function

  ! whether A aliases B
  logical function mat_is_alias(A, B)
    type(openrsp_matrix), target, intent(in) :: A, B
    mat_is_alias = associated(A%self_pointer, B)
  end function
  
  integer function mat_get_nrow(A)
    type(openrsp_matrix), intent(in) :: A
    mat_get_nrow = A%nrow
  end function

  integer function mat_get_ncol(A)
    type(openrsp_matrix), intent(in) :: A
    mat_get_ncol = A%ncol
  end function

  ! matrix dot product: sum_ij Aij Bij (t=F)
  !  or  product trace: sum_ij Aji Bij (t=T)
  ! A and B are assumed initialized and with equal/transpose shapes
  function mat_dot(A, B, t)
    type(openrsp_matrix), intent(in) :: A, B
    logical,      intent(in) :: t !transpose
    real(8) mat_dot
    integer i, j, iz
    real(8), external :: ddot
    call mat_assert_def(A, 'mat_dot(A,B) or mat_trace(A,B) called with matrix A undefined')
    call mat_assert_def(B, 'mat_dot(A,B) or mat_trace(A,B) called with matrix B undefined')
    mat_dot = 0.0d0
    if (t) then
       do j = 1, B%ncol
         do i = 1, B%nrow
            mat_dot = mat_dot + A%elms(j, i, 1)*B%elms(i, j, 1)
         end do
       end do
       if (mat_is_quaternion(A) .or. mat_is_quaternion(B)) then
          do iz = 2, 4
             do j = 1, B%ncol
                do i = 1, B%nrow
                   mat_dot = mat_dot - A%elms(j, i, iz)*B%elms(i, j, iz)
                end do
             end do
          end do
       end if
    else
       mat_dot = ddot(min(size(A%elms), size(A%elms)), &
                      A%elms, 1, B%elms, 1)
    end if
    ! if closed shell, add additional factor 2
    if (mat_is_closed_shell(A) .or. mat_is_closed_shell(B)) then
       mat_dot = 2.0d0*mat_dot
    end if
  end function

  ! matrix trace = sum Aii, for A square
  function mat_trace(A)
    type(openrsp_matrix), intent(in) :: A
    real(8) mat_trace
    integer i, iz
    call mat_assert_def(A, 'matrix mat_trace(A) called with A undefined')
    if (A%nrow /= A%ncol) call quit('matrix mat_trace(A) called with A non-square')
    mat_trace = 0
    if (.not.mat_is_zero(A)) &
       mat_trace = sum((/((A%elms(i,i,iz), i=1,A%nrow), iz=1,A%algebra)/))
    ! if closed shell, add additional factor 2
    if (mat_is_closed_shell(A)) mat_trace = 2*mat_trace
  end function

  ! private used by mat_axpy and mat_gemm to verify and prepare
  ! their inout args Y and C, respectively
  function prepare_inout_mat(nrow, ncol, zero, quaternion, equals, A)
    integer,      intent(in)    :: nrow, ncol
    logical,      intent(in)    :: zero, quaternion
    logical,      intent(inout) :: equals
    type(openrsp_matrix), intent(inout) :: A
    logical      prepare_inout_mat
    type(openrsp_matrix) reA
    ! verify current contents, if any
    if (mat_is_defined(A)) then
       prepare_inout_mat = (nrow == A%nrow .and. ncol == A%ncol)
       if (.not.equals .and. .not.prepare_inout_mat) return
       ! free if it is A=..., but current A has wrong shape, is non-zero when the result should
       ! be zero, is complex when the result is real, or is an alias
       if (equals .and. (.not.prepare_inout_mat &
                    .or. (zero .and. .not.mat_is_zero(A)) &
                    .or. (mat_is_quaternion(A) .and. .not.quaternion) &
                    .or. (.not.zero .and. .not.mat_is_alias(A, A)))) &
          call mat_remove(A)
    end if
    ! initialize if undefined
    if (.not.mat_is_defined(A)) then
       call mat_init(A, nrow, ncol, is_zero=zero, is_quaternion=quaternion)
    ! otherwise, no need to reallocate if 0(+)=0, A non-alias, or Aq+=0q
    else if ((zero .and. mat_is_zero(A)) .or. mat_is_alias(A, A) &
        .or. (zero .and. .not.(quaternion .and. .not.mat_is_quaternion(A)))) then
        ! if A zero, inherit any quaternion algebra
        if (mat_is_zero(A) .and. quaternion) A%algebra = 4
    ! reallocate if A alias or real when result complex
    else 
       if (mat_is_zero(A)) equals = .true.
       call mat_ensure_alloc(A, only_alloc = equals)
    end if
    prepare_inout_mat = .true.
  end function

  ! If the matrix is zero, allocate and zero elements. If it is an alias,
  ! allocate and copy. Otherwise it is allocated, so do nothing
  ! with only_alloc=T, copying/zeroing will be skipped
  subroutine mat_ensure_alloc(A, only_alloc)
    type(openrsp_matrix),      intent(inout) :: A
    logical, optional, intent(in)    :: only_alloc
    type(openrsp_matrix) B, Bb
    logical      no
    real(8)      fac
    call mat_assert_def(A, 'mat_ensure_alloc(A) called with matrix A undefined')
    ! if non-alias, return
    if (mat_is_alias(A, A)) return
    ! allocate B
    call mat_init(B, A%nrow, A%ncol, is_quaternion=mat_is_quaternion(A))
    ! decide whether or not to set the data in the newly allocated B (no)
    no = .false.
    if (present(only_alloc)) no = only_alloc
    ! decide which factor to use
    fac = merge(0, 1, mat_is_zero(A))
    ! make the copy (or zero). For quaternions do the four parts separately
    if (.not.no .and. mat_is_quaternion(A)) then
       Bb = mat_get_part(B, quat_i=F, quat_j=F)
       call axpy_real(fac, mat_get_part(A, quat_i=F, quat_j=F), .false., .true., Bb)
       call mat_set_part(Bb, B, quat_i=F, quat_j=F)
       Bb = mat_get_part(B, quat_i=T, quat_j=F)
       call axpy_real(fac, mat_get_part(A, quat_i=T, quat_j=F), .false., .true., Bb)
       call mat_set_part(Bb, B, quat_i=T, quat_j=F)
       Bb = mat_get_part(B, quat_i=F, quat_j=T)
       call axpy_real(fac, mat_get_part(A, quat_i=F, quat_j=T), .false., .true., Bb)
       call mat_set_part(Bb, B, quat_i=F, quat_j=T)
       Bb = mat_get_part(B, quat_i=T, quat_j=T)
       call axpy_real(fac, mat_get_part(A, quat_i=T, quat_j=T), .false., .true., Bb)
       call mat_set_part(Bb, B, quat_i=T, quat_j=T)
    else if (.not.no) then
       call axpy_real(fac, A, .false., .true., B)
    end if
    ! finally move the copy in B over A
    call mat_move(B, A)
  end subroutine

  ! place i*f*A in B. This is the only way to construct a complex
  ! matrix when you only have real ones.
  subroutine mat_imag_times_mat(f, A, B)
    real(8),              intent(in)    :: f
    type(openrsp_matrix), target, intent(in)    :: A
    type(openrsp_matrix), target, intent(inout) :: B
    call quit("mat_imag_times_mat called but complex matrices aren't yet implemented")
  end subroutine

  ! return alias to either real or imaginary part of A
  function mat_get_part(A, imag, quat_i, quat_j, beta) result(B)
    type(openrsp_matrix),      intent(in) :: A
    logical, optional, intent(in) :: imag, quat_i, quat_j, beta
    type(openrsp_matrix) B
    logical qi, qj
    integer i
    if (present(imag)) call quit( &
          "mat_get_part called with 'imag' present, which isn't implemented yet")
    if (present(beta)) call quit( &
          "mat_get_part called with 'beta' present, which isn't implemented yet")
    qi = .false.
    if (present(quat_i)) qi = quat_i
    qj = .false.
    if (present(quat_j)) qj = quat_j
    if (mat_is_zero(A) .or. (.not.mat_is_quaternion(A) .and. (qi.or.qj))) then
       call mat_init(B, A%nrow, A%ncol, is_zero=.true.) !zero=T, quat=F
    else
       call mat_dup(A, B)
       ! merge: qi qj = FF:1 TF:2 FT:3 TT:4
       i = 1 + merge(1,0,qi) + merge(2,0,qj)
       B%elms => A%elms(:,:,i:i)
       B%algebra = 1 !real
    end if
  end function


  ! re(B)=A or im(B)=A. A must be real, and is removed after use
  ! FIXME ajt For now without complex and open_shall support
  subroutine mat_set_part(A, B, imag, quat_i, quat_j, beta)
    type(openrsp_matrix), target, intent(inout) :: A, B
    logical,    optional, intent(in)    :: imag, quat_i, quat_j, beta
    type(openrsp_matrix), target :: C
    integer n, i
    logical qi, qj, useC, Bzero, Bquat
    if (present(imag)) call quit( &
          "mat_set_part called with 'imag' present, which isn't implemented yet")
    if (present(beta)) call quit( &
          "mat_set_part called with 'beta' present, which isn't implemented yet")
    ! A must be real
    if (mat_is_quaternion(A)) &
       call quit('mat_set_part called with A not real. A must be real')
    qi = .false.
    if (present(quat_i)) qi = quat_i
    qj = .false.
    if (present(quat_j)) qj = quat_j
    ! if B is zero or if B is real but will become quaternion, reallocate
    ! it, and copy/zero the real part
    Bzero = mat_is_zero(B)
    if ((mat_is_zero(A) .and. Bzero) .or. (.not.mat_is_quaternion(B) .and. (qi.or.qj))) then
       call mat_init(C, B%nrow, B%ncol, is_quaternion=(mat_is_quaternion(B) .or. qi .or. qj))
       if (.not.Bzero) C%elms(:,:,1) = B%elms(:,:,1)
       if (     Bzero) C%elms(:,:,1) = 0
       call mat_move(C, B)
    end if
    ! copy desired part from A, or zero
    if (mat_is_zero(A) .or. mat_is_quaternion(B)) then
       ! merge: qi qj = FF:1 TF:2 FT:3 TT:4
       i = 1 + merge(1,0,qi) + merge(2,0,qj)
       if (mat_is_zero(A)) then
          B%elms(:,:,i) = 0
       else if (.not.associated(A%elms, B%elms(:,:,i:i))) then
          B%elms(:,:,i) = A%elms(:,:,1)
       end if
       call mat_remove(A)
    else !or move the entire A, if setting the real part
       call mat_move(A, B)
    end if
  end subroutine


  ! turn a non-alias matrix into one with a temp tag
  subroutine mat_hide_temp(A)
    type(openrsp_matrix), target, intent(inout) :: A
    if (mat_is_alias(A, A)) A%magic_tag = magic_val_temp
  end subroutine

  ! turn a temp-tagged matrix into a non-alias one
  subroutine mat_unhide_temp(A)
    type(openrsp_matrix), target, intent(inout) :: A
    if (A%magic_tag == magic_val_temp) then
       A%magic_tag = magic_val_nonz
       A%self_pointer => A
    end if
  end subroutine

  function mat_acquire_block(A, minrow, maxrow, mincol, maxcol)
    type(openrsp_matrix), target :: A !no intent to allow use in intent(in) routines
    integer,  intent(in) :: minrow, maxrow, mincol, maxcol
    real(8),     pointer :: mat_acquire_block(:,:)
    if (A%magic_tag == magic_val_temp) then
       call quit('mat_acquire_block called with matrix A already acquired')
    else if (A%magic_tag == magic_val_zero) then
       call quit('mat_acquire_block called with matrix A zero')
    else if (A%magic_tag /= magic_val_nonz) then
       call quit('mat_acquire_block called with matrix A undefined')
    end if
    mat_acquire_block => A%elms(minrow : maxrow, mincol : maxcol, 1)
    A%magic_tag = magic_val_temp
  end function

  subroutine mat_unacquire_block(A, block)
    type(openrsp_matrix), target :: A !no intent to allow use in intent(in) routines
    real(8),     pointer :: block(:,:) !intent(inout), but pointer can't have intent
    if (A%magic_tag /= magic_val_temp) &
       call quit('mat_unacquire_block called with matrix A not already acquired')
    nullify(block)
    A%magic_tag = magic_val_nonz
  end subroutine

  !> \brief broadcasts matrix
  !> \author Bin Gao
  !> \date 2012-05-13
  !> \param A is the matrix
  !> \param root is the root processor which broadcasts the matrix
  !> \param mat_comm is the MPI communicator
  subroutine mat_mpi_bcast(A, root, mat_comm)
    type(openrsp_matrix), intent(inout) :: A
    integer,      intent(in)    :: root
    integer,      intent(in)    :: mat_comm
#ifdef VAR_MPI
#include "mpif.h"
    integer rank_proc  !rank of processor
    integer ierr       !error information
    logical zero, quat
    call MPI_Comm_rank(mat_comm, rank_proc, ierr)
    if (rank_proc == root) then
       call mat_assert_def(A, 'mat_mpi_bcast called with matrix A undefined')
       zero = mat_is_zero(A)
       quat = mat_is_quaternion(A)
    end if
    ! broadcast the matrix' shape and type
    call MPI_Bcast(A%nrow, 1, MPI_INTEGER, root, mat_comm, ierr)
    call MPI_Bcast(A%ncol, 1, MPI_INTEGER, root, mat_comm, ierr)
    call MPI_Bcast(zero,   1, MPI_LOGICAL, root, mat_comm, ierr)
    call MPI_Bcast(quat,   1, MPI_LOGICAL, root, mat_comm, ierr)
    ! initialize the matrix on slaves
    if (rank_proc /= root) &
       call mat_init(A, A%nrow, A%ncol, is_zero=zero, is_quaternion=quat)
    ! if nonzero, broadcast matrix elements
    if (.not.zero) call MPI_Bcast(A%elms, size(A%elms), MPI_DOUBLE_PRECISION, &
                                  root, mat_comm, ierr)
#endif
  end subroutine

end module
